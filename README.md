# vue-vector-pie-progress

This VueJS plugin adds a component to draw a SVG pie chart like progress with various parameters and gradient support.

![main](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/main.png "Main")

## Demo page

![sample](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/demo.png "Basic sample")

> <a href="https://csb-ohmeg.netlify.com/" target="_blank">Visit showcase</a>

> <a href="https://codesandbox.io/s/vue-template-ohmeg?fontsize=14" target="_blank">Open samples editor in codesandbox.io</a>

## Full docs

> <a href="https://mc2interaction.gitlab.io/vue-vector-pie-progress" target="_blank">See full docs on GitLab</a>

## Install

    npm i @mc2interaction/vue-vector-pie-progress --save
  
## Usage

    import VueVectorPieProgress from "@mc2interaction/vue-vector-pie-progress"

    Vue.use(VueVectorPieProgress)

Then include component in your Vue template

    <vector-pie-progress :value="48" />



