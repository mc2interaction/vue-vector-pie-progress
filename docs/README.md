# vue-vector-pie-progress

This VueJS plugin adds a component to draw a SVG pie chart like progress with various parameters and gradient support.

![main](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/main.png "Main")

## Demo page

![sample](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/demo.png "Basic sample")

> <a href="https://csb-ohmeg.netlify.com/" target="_blank">Visit showcase</a>

> <a href="https://codesandbox.io/s/vue-template-ohmeg?fontsize=14" target="_blank">Open samples editor in codesandbox.io</a>

## Install

    npm i @mc2interaction/vue-vector-pie-progress --save
  
## Usage

    import VueVectorPieProgress from "@mc2interaction/vue-vector-pie-progress"

    Vue.use(VueVectorPieProgress)

Then include component in your Vue template

    <vector-pie-progress :value="48" />
    
## Component properties

### Fill color

    fillColor: { type: String, default: '#f1f1f1' },

Color used to fill space between outer border and inner border.

    <template>
      <div>
        <div>
          <h2>Fill color (default)</h2>
          <vector-pie-progress :value="48"/>
        </div>
        <div>
          <h2>Fill color (black)</h2>
          <vector-pie-progress :value="48" fill-color="#000"/>
        </div>
        <div>
          <h2>Fill color (red)</h2>
          <vector-pie-progress :value="48" fill-color="#f00"/>
        </div>
      </div>
    </template>

![fill-color](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/fill-color.png "Fill color")

### Inner background colors

    innerBackgroundColors: {type: Array,
      default: () => [
        {offset: '0', stopColor: '#2268ff'},
        {offset: '100', stopColor: '#0d29ff'}
      ]
    }

Gradient colors for internal circle of progress. You can add as many offsets as you want.

Default values generate a linear gradient with 2 offsets.

    <linearGradient x1="0%" y1="0%" x2="0%" y2="100%" spreadMethod="pad">
      <stop offset="0%" stop-color="#2268ff"></stop>
      <stop offset="100%" stop-color="#0d29ff"></stop>
    </linearGradient>


    <template>
      <div>
        <div>
          <h2>Inner background colors (default)</h2>
          <vector-pie-progress :value="48"/>
        </div>
        <div>
          <h2>Inner background colors (2 colors gradient)</h2>
          <vector-pie-progress
            :value="48"
            :inner-background-colors="[{ offset: '0', stopColor: '#051937' }, { offset: '100', stopColor: '#A8EB12' }]"
          />
        </div>
        <div>
          <h2>Inner background colors (5 colors gradient)</h2>
          <vector-pie-progress
            :value="48"
            :inner-background-colors="[
              { offset: '0', stopColor: '#051937' }, 
              { offset: '25', stopColor: '#004d7a' }, 
              { offset: '50', stopColor: '#008793' }, 
              { offset: '75', stopColor: '#00bf72' }, 
              { offset: '100', stopColor: '#a8eb12' }
            ]"
          />
        </div>
      </div>
    </template>

![inner-background-colors](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/inner-background-colors.png "Inner background colors")

### Inner border color

    innerBorderColor: { type: String, default: '#f1f1f1' }

Color of internal border between internal circle and filled space. 

    <template>
      <div>
        <div>
          <h4>Inner border color (default)</h4>
          <vector-pie-progress :value="48"/>
        </div>
        <div>
          <h4>Inner border color (black)</h4>
          <vector-pie-progress :value="48" inner-border-color="#000"/>
        </div>
        <div>
          <h4>Inner border color (red)</h4>
          <vector-pie-progress :value="48" inner-border-color="#f00"/>
        </div>
      </div>
    </template>

![inner-border-color](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/inner-border-color.png "Inner border color")

### Inner border width

    innerBorderWidth: { type: Number, default: 3 }

Width of internal border between internal circle and filled space.

    <template>
      <div>
        <div>
          <h4>Inner border width (default)</h4>
          <vector-pie-progress :value="48"/>
        </div>
        <div>
          <h4>Inner border width (1)</h4>
          <vector-pie-progress :value="48" :inner-border-width="1"/>
        </div>
        <div>
          <h4>Inner border width (10)</h4>
          <vector-pie-progress :value="48" :inner-border-width="10"/>
        </div>
      </div>
    </template>

![inner-border-width](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/inner-border-width.png "Inner border width")

### Inner font size

    innerFontSize: { type: String, default: '0.68rem' }

Font size for value text if displayed (CSS style value)

    <template>
      <div>
        <div>
          <h4>Inner font size (default)</h4>
          <vector-pie-progress :value="48"/>
        </div>
        <div>
          <h4>Inner font size (0.50rem)</h4>
          <vector-pie-progress :value="48" inner-font-size="0.50rem"/>
        </div>
        <div>
          <h4>Inner font size (0.9rem)</h4>
          <vector-pie-progress :value="48" inner-font-size="0.9rem"/>
        </div>
      </div>
    </template>

![inner-font-size](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/inner-font-size.png "Inner font size")

### Inner font weight

    innerFontWeight: { type: String, default: 'bold' }

Font weight for value text if displayed (CSS style value)

    <template>
      <div>
        <div>
          <h4>Inner font weight (default)</h4>
          <vector-pie-progress :value="48" inner-font-size="0.9rem"/>
        </div>
        <div>
          <h4>Inner font weight (normal)</h4>
          <vector-pie-progress :value="48" inner-font-size="0.9rem" inner-font-weight="normal"/>
        </div>
        <div>
          <h4>Inner font weight (lighter)</h4>
          <vector-pie-progress :value="48" inner-font-size="0.9rem" inner-font-weight="lighter"/>
        </div>
      </div>
    </template>

![inner-font-weight](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/inner-font-weight.png "Inner font weight")

### Inner circle class

    innerCircleClass: { type: String, default: null }

Additionnal CSS class for inner circle

    <template>
      <div>
        <div>
          <h4>Inner circle class (default)</h4>
          <vector-pie-progress :value="48"/>
        </div>
        <div>
          <h4>Inner circle class (0.7 opacity)</h4>
          <vector-pie-progress :value="48" inner-circle-class="opacity-70"/>
        </div>
        <div>
          <h4>Inner circle class (0.1 opacity)</h4>
          <vector-pie-progress :value="48" inner-circle-class="opacity-10"/>
        </div>
      </div>
    </template>

    <style>
    .opacity-70 {
      fill-opacity: 0.7;
    }

    .opacity-10 {
      fill-opacity: 0.1;
    }
    </style>

![inner-circle-class](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/inner-circle-class.png "Inner circle class")

### Inner text class

    innerTextClass: { type: String, default: null }

Additionnal CSS class for inner text

    <template>
      <div>
        <div>
          <h4>Inner text class (default)</h4>
          <vector-pie-progress :value="48"/>
        </div>
        <div>
          <h4>Inner text class (0.7 opacity)</h4>
          <vector-pie-progress :value="48" inner-text-class="opacity-70"/>
        </div>
        <div>
          <h4>Inner text class (0.4 opacity)</h4>
          <vector-pie-progress :value="48" inner-text-class="opacity-40"/>
        </div>
      </div>
    </template>

    <style>
    .opacity-70 {
      fill-opacity: 0.7;
    }

    .opacity-40 {
      fill-opacity: 0.4;
    }
    </style>

![inner-text-class](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/inner-text-class.png "Inner text class")

### Inner text color

    innerTextColor: { type: String, default: '#f1f1f1' }

Inner text color

### Max. value

    maxalue: { type: Number, default: 100 }

Progress maximum value. If `percent=true` value is displayed as percent according to this parameter.

    <vector-pie-progress :value="200" :max-value="500" />

### Outer background colors

    outerBackgroundColors: {type: Array,
      default: () => [
        {offset: '0', stopColor: '#39f'},
        {offset: '100', stopColor: '#00f'}
      ]
    }

Default gradient colors for external circle of progress. You can add as many offsets as you want.

Default values generate a linear gradient with 2 offsets.

    <linearGradient x1="0%" y1="0%" x2="0%" y2="100%" spreadMethod="pad">
      <stop offset="0%" stop-color="#39f"></stop>
      <stop offset="100%" stop-color="#00f"></stop>
    </linearGradient>

If you add more offsets you can generate a smoother linear gradient

    <vector-pie-progress :outer-background-colors="
      [
        { offset: "0", stopColor: "#3dc669" },
        { offset: "20", stopColor: "#00af81" },
        { offset: "40", stopColor: "#00968c" },
        { offset: "60", stopColor: "#007b88" },
        { offset: "80", stopColor: "#076175" },
        { offset: "100", stopColor: "#2f4858" }
      ]
    " />

gives a linear gradient with 6 offsets

    <linearGradient x1="0%" y1="0%" x2="0%" y2="100%" spreadMethod="pad">
      <stop offset="0%" stop-color="#3dc669"></stop>
      <stop offset="20%" stop-color="#00af81"></stop>
      <stop offset="40%" stop-color="#00968c"></stop>
      <stop offset="60%" stop-color="#007b88"></stop>
      <stop offset="80%" stop-color="#076175"></stop>
      <stop offset="100%" stop-color="#2f4858"></stop>
    </linearGradient>

### Outer border width

    outerBorderWidth: { type: Number}

Width for external circle of progress. Default is `3` or `1` when `small=true`

    <template>
      <div>
        <div>
          <h4>Outer border width (default)</h4>
          <vector-pie-progress :value="48"/>
        </div>
        <div>
          <h4>Outer border width (1)</h4>
          <vector-pie-progress :value="48" :outer-border-width="1"/>
        </div>
        <div>
          <h4>Outer border width (10)</h4>
          <vector-pie-progress :value="48" :outer-border-width="10"/>
        </div>
      </div>
    </template>

![outer-border-width](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/outer-border-width.png "Outer border width")

### Outer circle class

    outerCircleClass: { type: String, default: null }

Additionnal CSS class for outer circle

    <template>
      <div>
        <div>
          <h4>Outer circle class (default)</h4>
          <vector-pie-progress :value="48"/>
        </div>
        <div>
          <h4>Outer circle class (0.5 opacity)</h4>
          <vector-pie-progress :value="48" outer-circle-class="opacity-50"/>
        </div>
        <div>
          <h4>Outer circle class (0.3 opacity)</h4>
          <vector-pie-progress :value="48" outer-circle-class="opacity-30"/>
        </div>
      </div>
    </template>

    <style>
    .opacity-50 {
      stroke-opacity: 0.5;
    }

    .opacity-30 {
      stroke-opacity: 0.3;
    }
    </style>

![outer-circle-class](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/outer-circle-class.png "Outer circle class")

### Path class

    pathClass: { type: String, default: null },

Additionnal CSS class for pie path

    <template>
      <div>
        <div>
          <h4>Path class (default)</h4>
          <vector-pie-progress :value="48"/>
        </div>
        <div>
          <h4>Path class (0.7 opacity)</h4>
          <vector-pie-progress :value="48" path-class="opacity-70"/>
        </div>
        <div>
          <h4>Path class (0.1 opacity)</h4>
          <vector-pie-progress :value="48" path-class="opacity-30"/>
        </div>
      </div>
    </template>

    <style>
    .opacity-70 {
      fill-opacity: 0.7;
    }

    .opacity-30 {
      fill-opacity: 0.3;
    }
    </style>

![path-class](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/path-class.png "Path class")

### Percent

    percent: { type: Boolean, default: true }

Show value in percent of `maxValue` or in absolute value.

### Show value

    showValue: { type: Boolean, default: true }

Show or hide value in inner circle

### Small

    small: { type: Boolean, default: false }

Shortcut property to display progress as a small SVG with default behaviour (like do not display text, ...)

### Radius

    radius: { type: Number, default: 25 or 10 when small }

Changes progress radius i.e global size of component

    <template>
      <div>
        <div>
          <h2>Radius (default)</h2>
          <vector-pie-progress :value="48"/>
        </div>
        <div>
          <h2>Radius (small)</h2>
          <vector-pie-progress :value="48" small/>
        </div>
        <div>
          <h2>Radius 50</h2>
          <vector-pie-progress :value="48" :radius="50"/>
        </div>
      </div>
    </template>

![radius](https://gitlab.com/mc2interaction/vue-vector-pie-progress/raw/master/docs/images/radius.png "Radius")

### Value

    value: { type: Number, default: 0 }

Current progress value. If `percent=true` value is displayed as percent according to `max-value`.

    <vector-pie-progress :value="72" />
