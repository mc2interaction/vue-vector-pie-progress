import VectorPieProgress from './src/components/VectorPieProgress.vue'

// This exports the plugin object.
const VueVectorPieProgress = {
  // The install method will be called with the Vue constructor as         
  // the first argument, along with possible options
  install (Vue, options) {
     Vue.component('vector-pie-progress', VectorPieProgress)
  }
}

export default VueVectorPieProgress